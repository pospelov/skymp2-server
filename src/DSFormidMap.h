#pragma once
#include <vector>
#include <map>

using segment = uint8_t; //0x01, 0x02, 0xFF

// 0xFF000123 => 0xFF000000
// 0x0173821A => 0x01000000
inline segment GetSegment(formid id) {
	return (id - (id % 0x1000000)) / 0x1000000;
}

template <class T>
class DSFormidMap
{
public:
	// O(1)
	T &operator[](formid id) {
		auto &vec = segments[GetSegment(id)];
		const auto idx = id - GetSegment(id) * 0x1000000;
		if (vec.size() <= idx) vec.resize(idx + 1);

		auto &e = Exist(id);
		if (!e) {
			e = true;
			keys.push_back(id);
			size++;
		}

		return vec[idx];
	}

	// O(1) 
	std::vector<formid> GetKeys() const {
		return keys;
	}

	// O(???) 
	void Erase(formid id) {
		(*this)[id] = T();
		if (Exist(id)) {
			size--;
		}
		Exist(id) = false;
		auto &vec = exist[GetSegment(id)];
		vec.erase(std::remove(vec.begin(), vec.end(), id), vec.end());
		keys.erase(std::remove(keys.begin(), keys.end(), id), keys.end());
	}

	size_t GetSize() const {
		return size;
	}

private:
	uint8_t &Exist(formid id) {
		auto &vec = exist[GetSegment(id)];
		const auto idx = id - GetSegment(id) * 0x1000000;
		if (vec.size() <= idx) vec.resize(idx + 1);
		return vec[idx];
	}

	std::array<std::vector<T>, 0x100> segments;
	std::array<std::vector<uint8_t>, 0x100> exist;
	std::vector<formid> keys;
	size_t size = 0;
};