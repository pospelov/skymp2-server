#pragma once

class FormManager;
struct FormData;
class TESForm;

class FormManager : noncopyable
{
public:
	FormManager();
	~FormManager();

	TESForm *operator[](formid formid) const;
	size_t GetNumForms() const;
	vector<formid> GetFormIDs(string type = "") const;

	void Register(std::unique_ptr<TESForm> form);

private:
	friend class TESForm;

	struct Impl;
	Impl *pImpl;
};