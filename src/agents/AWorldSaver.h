#pragma once
#include "../stdafx.h"

class AWorldSaver : noncopyable
{
public:
	AWorldSaver();
	~AWorldSaver();

private:
	struct Impl;
	Impl *pImpl;
};