#include "../stdafx.h"
#include "AWorldSaver.h"

struct AWorldSaver::Impl
{
};

AWorldSaver::AWorldSaver() {
	pImpl = new Impl;
}

AWorldSaver::~AWorldSaver() {
	delete pImpl; pImpl = nullptr;
}

