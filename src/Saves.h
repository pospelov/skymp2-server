#pragma once

class TESForm;

struct TypeAndFlags
{
	string type;
	uint32_t flags = 0;
};

inline bool operator==(const TypeAndFlags &lhs, const TypeAndFlags &rhs) {
	return lhs.type == rhs.type && lhs.flags == rhs.flags;
}

class Saves : noncopyable
{
public:

	using Map = std::map<string, json>;

	Saves(Map data);
	~Saves();

	Map CommitChanges();
	TypeAndFlags GetTypeAndFlags(formid id) const;

	std::set<formid> GetFormIDs();
	void InsertFormFields(formid id, json fields, TypeAndFlags typeAndFlags);
	std::map<formid, json> GetFieldsForAll() const;
	void Erase(formid id); // TODO: Test it

private:
	void InitTypesAndFlags();

	struct Impl;
	Impl *pImpl;
};