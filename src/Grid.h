#pragma once

class Grid : noncopyable
{
public:
	Grid();
	~Grid();

	// ����������� id �� ����� ���������� newX, newY
	// ��������, �� ���� id ����� � ������ ���
	void Move(formid id, int16_t newX, int16_t newY);

	// �������� ���������� ������� id
	// ���� id �� ������, ������� ����������
	std::pair<int16_t, int16_t> GetPos(formid id) const;

	// ������ id, ����� ��� �� ���� �� �� ����� �����������
	// ���� id �� ������, ������ �� ������
	void Forget(formid id);

	// �������� ���� ������� ������� id
	// ������� ��������� ��� id, �� ������� ���������� <= 1. �� ��������� ���� ���������
	// ��������� ��������� O(1)
	std::set<formid> GetNeighbours(formid id) const;

private:
	struct Impl;
	Impl *pImpl;
};