#include "stdafx.h"
#include "Grid.h"
#include"DSFormidMap.h"
#include"DSLine.h"
// TODO: ���������� ����, ������ �������, �������� �������� � ������� �����
// TODO: Grid::Move() �� O(1) � ������� DSLine
// TODO: �������������� ��� ������ � ������� ���-��� ������ formid (>100�)

struct Obj
{
	bool active = 0;
	std::pair<int16_t, int16_t> coords;
	//std::set<formid> neighbours; //TODO: std::set instead of vector
	bool isEmpty()
	{
		return !active;
	}
	Obj()
	{

	}
};
bool IsNeighbours(int16_t x1, int16_t y1, int16_t x2, int16_t y2)
{
	if (x1 <= x2 + 1 && x1 >= x2 - 1 && y1 <= y2 + 1 && y1 >= y2 - 1)
		return true;

	return false;
}
struct Grid::Impl 
{
	DSFormidMap<Obj> objects;
	DSLine<DSLine<std::set<formid>>> cells;
};

Grid::Grid() {
	pImpl = new Impl;
}

Grid::~Grid() {
	delete pImpl; pImpl = nullptr;
}

void Grid::Move(formid id, int16_t x, int16_t y) {
	if (pImpl->objects[id].isEmpty())
	{
		pImpl->objects[id].active = true;
		pImpl->objects[id].coords = std::pair<int16_t, int16_t>(x, y);
		pImpl->cells[x][y].insert(id);
	}
	else
	{		
		pImpl->cells[pImpl->objects[id].coords.first][pImpl->objects[id].coords.second].erase(id);
		pImpl->objects[id].coords = std::pair<int16_t, int16_t>(x, y);
		pImpl->cells[x][y].insert(id);
	}
}

std::pair<int16_t, int16_t> Grid::GetPos(formid id) const {
	if (!pImpl->objects[id].isEmpty()) return pImpl->objects[id].coords;
	throw std::logic_error("grid: id not found");
}

void Grid::Forget(formid id) {
	if (!pImpl->objects[id].isEmpty())
	{
		pImpl->objects[id].active = false;
		pImpl->cells[pImpl->objects[id].coords.first][pImpl->objects[id].coords.second].erase(id);
		pImpl->objects.Erase(id);
	}	
}

std::set<formid> Grid::GetNeighbours(formid id) const {
	if (!pImpl->objects[id].isEmpty())
	{
		std::set<formid> result;
		int16_t myX = pImpl->objects[id].coords.first;
		int16_t myY = pImpl->objects[id].coords.second;
		for (int16_t x = myX - 1; x <= myX + 1; x++)
		{
			for (int16_t y = myY - 1; y <= myY + 1; y++)
			{
				result.insert(pImpl->cells[x][y].begin(), pImpl->cells[x][y].end());
			}
		}
		result.erase(id);
		return result;
	}
	return {};
}