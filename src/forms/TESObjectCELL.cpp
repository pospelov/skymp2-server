#include "../stdafx.h"
#include "TESObjectCELL.h"

struct TESObjectCELL::Impl
{
	json data = json::object();

	bool isInterior() { return  data["interior"]; }
	bool isPublicArea() { return  data["publicArea"]; }
	std::pair<int16_t, int16_t> GetXY() { return  { data["x"], data["y"] }; }
	formid GetLocation() { return data["locationid"]; };
};

TESObjectCELL::TESObjectCELL(formid id, uint32_t flags) : TESForm(id, flags) {
	pImpl.reset(new Impl);
}

TESObjectCELL::~TESObjectCELL() {

}

void TESObjectCELL::Load(json from) {
	extendJson(pImpl->data, from);
}

void TESObjectCELL::Save(json *to) {
	*to = pImpl->data;
}