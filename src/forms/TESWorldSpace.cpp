#include "../stdafx.h"
#include "TESWorldSpace.h"

struct TESWorldSpace::Impl
{
	size_t numSaves = 0;
};

TESWorldSpace::TESWorldSpace(formid id, uint32_t flags) : TESForm(id, flags) {
	pImpl.reset(new Impl);
}

TESWorldSpace::~TESWorldSpace() {

}

void TESWorldSpace::Load(json from) {
	if (from["numSaves"].is_number_unsigned())
		pImpl->numSaves = from["numSaves"].get<size_t>();
}

void TESWorldSpace::Save(json *to) {
	pImpl->numSaves++;
	(*to)["numSaves"] = pImpl->numSaves;
}