#include "../stdafx.h"
#include "TESObjectREFR.h"

struct TESObjectREFR::Impl
{
	bool loadedOnce = false;
	string edid;
	formid baseid = 0;
	formid cellid = 0;
	NiPoint3 pos;
	NiPoint3 rotRad;
};

TESObjectREFR::TESObjectREFR(formid id, uint32_t flags) : TESForm(id, flags) {
	pImpl.reset(new Impl);
}

TESObjectREFR::~TESObjectREFR() {

}

void TESObjectREFR::Load(json from) {
	for (const auto &j : json::iterator_wrapper(from)) {
		if (j.key() == "edid") pImpl->edid = j.value().get<string>();
		if (j.key() == "baseid") pImpl->baseid = j.value().get<formid>();
		if (j.key() == "parentid") pImpl->cellid = j.value().get<formid>();
		if (j.key() == "pos") pImpl->pos = j.value().get<NiPoint3>();
		if (j.key() == "rot") pImpl->rotRad = j.value().get<NiPoint3>();
	}
}

void TESObjectREFR::Save(json *to) {
	auto j = json::object();
	if (pImpl->edid.size() != 0) j["edid"] = pImpl->edid;
	//j["baseid"] = pImpl->baseid; // �� ���� ���������? baseid �� ����� ����������
	j["parentid"] = pImpl->cellid;
	j["pos"] = pImpl->pos;
	j["rot"] = pImpl->rotRad;
	*to = j;
}