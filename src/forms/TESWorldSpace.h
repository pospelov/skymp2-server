#pragma once
#include "TESForm.h"

class TESWorldSpace : public TESForm
{
public:
	TESWorldSpace(formid id, uint32_t flags);
	virtual ~TESWorldSpace();

	string GetType() const override { return "WRLD"; };

	void Load(json from) override;
	void Save(json *to) override;

private:
	struct Impl;
	unique_ptr<Impl> pImpl;
};