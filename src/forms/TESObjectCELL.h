#pragma once
#include "TESForm.h"

class TESObjectCELL : public TESForm
{
public:
	TESObjectCELL(formid id, uint32_t flags);
	virtual ~TESObjectCELL();

	string GetType() const override { return "CELL"; };

	void Load(json from) override;
	void Save(json *to) override;

private:
	struct Impl;
	unique_ptr<Impl> pImpl;
};