#pragma once
#include "TESForm.h"

class TESObjectREFR : public TESForm
{
public:
	TESObjectREFR(formid id, uint32_t flags);
	virtual ~TESObjectREFR();

	string GetType() const override { return "REFR"; };

	void Load(json from) override;
	void Save(json *to) override;

private:
	struct Impl;
	unique_ptr<Impl> pImpl;
};