#pragma once

class TESForm
{
public:
	TESForm(formid id, uint32_t flags);
	virtual ~TESForm();

	const formid id;
	const uint32_t flags;

	// fields - ��� ������, ��� ���� - �������� ����, � �������� - ��������
	// ��������������, ��� ������������ ������� ��� �������� �� Skyrim.esm, ����� �� .esp ��� ������ ������-������ (��������), � ����� �� ������ �������
	// �� ���� ���������� Load �� ������ �������� ����� ������ ����� ��� ������ ������. ��� ������ �� ���������.
	virtual void Load(json fields) = 0;

	// TODO: Document method behavior
	virtual void Save(json *fieldsOut) = 0;

	virtual string GetType() const = 0;
};