#pragma once

#include <array>
#include <map>
#include <set>
#include <memory>
#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include <thread>
#include <chrono>
#include <functional>
#include <optional>
#include <mutex>

#include <boost/noncopyable.hpp>
#include <boost/filesystem.hpp>
#include <boost/lockfree/queue.hpp>

#include "../libs/json/single_include/nlohmann/json.hpp"
#include "../libs/libespm/src/libespm.h"
#include "../RakNet/src/BitStream.h"

#include "NiPoint3.h"

inline void to_json(nlohmann::json &j, NiPoint3 &p) {
	auto res = nlohmann::json::object();
	res["x"] = p.x;
	res["y"] = p.y;
	res["z"] = p.z;
	std::swap(j, res);
}

inline void from_json(const nlohmann::json &j, NiPoint3 &p) {
	p.x = j.at("x").get<float>();
	p.y = j.at("y").get<float>();
	p.z = j.at("z").get<float>();
}

#define STATIC_ASSERT(x) static_assert(x, #x)

namespace fs = boost::filesystem;
using json = nlohmann::json;

template <class T> using unique_ptr = std::unique_ptr<T>;
template <class T> using shared_ptr = std::shared_ptr<T>;
template <class T> using vector = std::vector<T>;
template <class T, size_t size> using array = std::array<T, size>;
template <class T> using set = std::set<T>;
template <class T> using function = std::function<T>;
using string = std::string;
using noncopyable = boost::noncopyable;
using BitStream = RakNet::BitStream;

using namespace std::chrono_literals;

using formid = uint32_t; 
using TESFile = espm::tes5::File;

#if defined _WIN32 || _WIN64
#pragma comment(lib, "Ws2_32.lib")
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

#include "../libs/Catch2/single_include/catch2/catch.hpp"

// https://github.com/nlohmann/json/issues/428
inline void extendJson(json &j1, const json &j2) {
	if (j2.is_object() == false) return; // paranoia
	for (const auto &j : json::iterator_wrapper(j2)) {
		j1[j.key()] = j.value();
	}
}

// https://stackoverflow.com/questions/5100718/integer-to-hex-string-in-c
template <typename I> inline string to_string_hex(I w, size_t hex_len = sizeof(I) << 1) {
	auto digits = "0123456789ABCDEF";
	string rc(hex_len, '0');
	for (size_t i = 0, j = (hex_len - 1) * 4; i<hex_len; ++i, j -= 4)
		rc[i] = digits[(w >> j) & 0x0f];
	return rc;
}