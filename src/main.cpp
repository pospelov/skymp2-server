#include "stdafx.h"
#include "tests\tests.h"

#include "Server.h"

int main(int argc, char *argv[]) {
	printf("Running tests\n");
	const auto was = std::chrono::high_resolution_clock::now();
	tests_main(argc, argv);
	const auto time = std::chrono::high_resolution_clock::now() - was;
	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(time).count() << " ms" << std::endl;

	std::unique_ptr<Server> server;

	try {
		Config cfg;
		cfg.maxPlayers = 2000;
		cfg.port = 7777;

		server.reset(new Server(cfg));

		while (1) {
			std::this_thread::sleep_for(1ms);
			server->Tick();
		}
	}
	catch (std::exception &e) {
		printf("Exception:\n%s\n", e.what());
	}
	system("pause");
	return 0;
}