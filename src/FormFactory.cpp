#include "stdafx.h"
#include "FormFactory.h"

#include "FormData.h"

#include "forms/TESWorldSpace.h"
#include "forms/TESObjectCELL.h"
#include "forms/TESObjectREFR.h"

unique_ptr<TESForm> FormFactory::NewEmptyForm(const string &type, formid id, uint32_t flags) {

	unique_ptr<TESForm> res;

	//printf("%s\n",formData.type.data());
	if (type == "WRLD") {
		res.reset(new TESWorldSpace(id, flags));
	}
	else if (type == "CELL") {
		res.reset(new TESObjectCELL(id, flags));
	}
	else if (type == "REFR") {
		res.reset(new TESObjectREFR(id, flags));
	}
	else if (type == "ACHR") {

	}

	return res;
}