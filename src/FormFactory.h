#pragma once

class TESForm;
struct FormData;

namespace FormFactory
{
	// Creates an empty form. formData.fields are not used
	unique_ptr<TESForm> NewEmptyForm(const string &type, formid id, uint32_t flags);
}