#pragma once

struct FormData
{
	formid id = 0;
	std::string type;
	uint32_t flags = 0;
	json fields = json::object();
};