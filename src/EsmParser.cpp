#include "stdafx.h"
#include "EsmParser.h"

void EsmParser::ParseGroup(espm::Group &src, Callback c) {
	for (auto &group : src.subgroups) {
		ParseGroup(group, c);
	}
	for (auto &record : src.records) {
		ParseRecord(record, src.label, c);
	}
}

void EsmParser::ParseRecord(espm::Record &rec, void *parentlabel, Callback c) {

	FormData res;
	auto fields = json::object();
	res.flags = rec.flags;
	res.id = rec.id;
	res.type = string(rec.type, 4);

	auto type = string(rec.type, 4);

	//printf("%s\n", type.data());
	if (type == "WRLD") {
		for (auto &field : rec.fields) {
			// ...
		}
	}
	else if (type == "CELL") {
		fields["parentid"] = *(formid *)parentlabel;
		for (auto &field : rec.fields) {
			if (!memcmp(field.type, "DATA", 4)) {
				fields["interior"] = bool(*field.data & 0x0001);
				fields["publicArea"] = bool(*field.data & 0x0020);
				// ...
			}
			if (!memcmp(field.type, "XCLC", 4)) {
				fields["x"] = reinterpret_cast<int32_t *>(field.data)[0];
				fields["y"] = reinterpret_cast<int32_t *>(field.data)[1];
			}
			if (!memcmp(field.type, "XLCN", 4)) {
				fields["locationid"] = reinterpret_cast<formid *>(field.data)[0];
			}
		}
	} 
	else if (type == "REFR" || type == "ACHR") {
		fields["parentid"] = *(formid *)parentlabel;
		for (auto &field : rec.fields) {
			if (!memcmp(field.type, "NAME", 4)) {
				fields["baseid"] = reinterpret_cast<formid *>(field.data)[0];
			}
			if (!memcmp(field.type, "DATA", 4)) {
				fields["pos"] = reinterpret_cast<NiPoint3 *>(field.data)[0];
				fields["rot"] = reinterpret_cast<NiPoint3 *>(field.data)[1];
			}
		}
	}
	else return;

	for (auto &field : rec.fields) {
		if (!memcmp(field.type, "EDID", 4)) {
			fields["edid"]= (string)field.data;
		}
	}

	res.fields = fields;

	c(res);
}

void EsmParser::Parse(TESFile &file, Callback c) {
	for (auto &group : file.groups) ParseGroup(group, c);
}