#pragma once

template <class T>
class DSLine
{
public:
	T &operator[](int64_t index) {
		const size_t realIdx = index >= 0 ? index : -index;
		std::vector<T> &vec = index >= 0 ? positive : negative;
		if (vec.size() <= realIdx) vec.resize(realIdx + 1);
		return vec[realIdx];
	}

private:
	std::vector<T> positive; // >= 0
	std::vector<T> negative; // < 0
};