#pragma once

#include <type_traits>
#include <utility>
#include <list>
#include <string>
#include <vector>
#include <array>
#include <set>
#include <map>
#include <queue>
#include <deque>
#include <forward_list>
#include <memory>

#ifndef __BITSTREAM_H
#error "include BitStream.h before skyseri.h"
#endif

namespace skyseri
{

	using BitStream = RakNet::BitStream;

	template <class T, class ... Ts> 
	inline void write(BitStream &bs, T arg, Ts ... args) {
		write_impl(bs, arg);
		write(bs, args ...);
	}
	inline void write(BitStream &bs) {
	}

	template <class T>
	inline void write_impl(BitStream &bs, const T &val) {
		bs.Write(val);
	}
	template <class T>
	inline void write_impl(BitStream &bs, const std::basic_string<T> &val) {
		bs.Write((uint64_t)val.size()); for (auto &e : val) write_impl(bs, e);
	}
	template <class T>
	inline void write_impl(BitStream &bs, const std::vector<T> &val) {
		bs.Write((uint64_t)val.size()); for (auto &e : val) write_impl(bs, e);
	}
	template <class T>
	inline void write_impl(BitStream &bs, const std::list<T> &val) {
		bs.Write((uint64_t)val.size()); for (auto &e : val) write_impl(bs, e);
	}
	template <class T, class T2>
	inline void write_impl(BitStream &bs, const std::map<T, T2> &val) {
		bs.Write((uint64_t)val.size()); for (auto &e : val) write_impl(bs, e);
	}
	template <class T, class T2>
	inline void write_impl(BitStream &bs, const std::pair<T, T2> &val) {
		write_impl(bs, val.first);
		write_impl(bs, val.second);
	}
	template <class T>
	inline void write_impl(BitStream &bs, const std::set<T> &val) {
		bs.Write((uint64_t)val.size()); for (auto &e : val) write_impl(bs, e);
	}
	template <class T>
	inline void write_impl(BitStream &bs, const std::forward_list<T> &val) {
		auto copy = val;
		uint64_t valSize = 0; while (!copy.empty()) { copy.pop_front(); ++valSize; }
		bs.Write(valSize); for (auto &e : val) write_impl(bs, e);
	}
	template <class T>
	inline void write_impl(BitStream &bs, const std::queue<T> &val) {
		auto copy = val;
		std::vector<T> vec;
		uint64_t valSize = 0; while (!copy.empty()) { vec.push_back(copy.front()); copy.pop(); ++valSize; }
		bs.Write(valSize); for (auto &e : vec) write_impl(bs, e);
	}
	template <class T>
	inline void write_impl(BitStream &bs, const std::deque<T> &val) {
		bs.Write((uint64_t)val.size()); for (auto &e : val) write_impl(bs, e);
	}
	inline void write_impl(BitStream &bs, const char *val) {
		bs.Write((uint64_t)strlen(val)); for (size_t i = 0; val[i]; ++i) write_impl(bs, val[i]);
	}
	inline void write_impl(BitStream &bs, const wchar_t *val) {
		bs.Write((uint64_t)wcslen(val)); for (size_t i = 0; val[i]; ++i) write_impl(bs, val[i]);
	}
	template <class T, size_t size>
	inline void write_impl(BitStream &bs, const std::array<T, size> &val) {
		bs.Write((uint64_t)val.size()); for (auto &e : val) write_impl(bs, e);
	}
	template <class T>
	inline void write_impl(BitStream &bs, const std::unique_ptr<T> &val) {
		if (val) { bs.Write((char)1); write_impl(bs, *val); }
		else { bs.Write((char)0); }
	}
	template <class T>
	inline void write_impl(BitStream &bs, const std::shared_ptr<T> &val) {
		if (val) { bs.Write((char)1); write_impl(bs, *val); }
		else { bs.Write((char)0); }
	}

	/////////////////////////////////////////////////////////////////////



	template <class T, class ... Ts>
	inline void read(BitStream &bs, T &arg, Ts &... args) {
		read_impl(bs, arg);
		read(bs, args ... );
	}
	inline void read(BitStream &bs) {
	}

	template <class T>
	inline void read_impl(BitStream &bs, T &val) {
		if (!bs.Read(val)) {
			throw std::logic_error("unable to read");
		}
	}
	template <class T>
	inline void read_impl(BitStream &bs, std::basic_string<T> &val) {
		val.clear();
		uint64_t sz;
		bs.Read(sz);
		for (uint64_t i = 0; i != sz; ++i) {
			T el;
			read_impl(bs, el);
			val.push_back(el);
		}
	}
	template <class T>
	inline void read_impl(BitStream &bs, std::vector<T> &val) {
		val.clear();
		uint64_t sz;
		bs.Read(sz);
		for (uint64_t i = 0; i != sz; ++i) {
			T el;
			read_impl(bs, el);
			val.push_back(el);
		}
	}
	template <class T>
	inline void read_impl(BitStream &bs, std::list<T> &val) {
		val.clear();
		uint64_t sz;
		bs.Read(sz);
		for (uint64_t i = 0; i != sz; ++i) {
			T el;
			read_impl(bs, el);
			val.push_back(el);
		}
	}
	template <class T, class T2>
	inline void read_impl(BitStream &bs, std::map<T, T2> &val) {
		val.clear();
		uint64_t sz;
		bs.Read(sz);
		for (uint64_t i = 0; i != sz; ++i) {
			std::pair<T, T2> el;
			read_impl(bs, el);
			val.insert(el);
		}
	}
	template <class T, class T2>
	inline void read_impl(BitStream &bs, std::pair<T, T2> &val) {
		read_impl(bs, val.first);
		read_impl(bs, val.second);
	}
	template <class T>
	inline void read_impl(BitStream &bs, std::set<T> &val) {
		val.clear();
		uint64_t sz;
		bs.Read(sz);
		for (uint64_t i = 0; i != sz; ++i) {
			T el;
			read_impl(bs, el);
			val.insert(el);
		}
	}
	template <class T, size_t size>
	inline void read_impl(BitStream &bs, std::array<T, size> &val) {
		uint64_t sz;
		bs.Read(sz);
		if (sz != size) throw std::runtime_error("real array size is " + std::to_string(sz));
		for (uint64_t i = 0; i != sz; ++i) {
			read_impl(bs, val[i]);
		}
	}
	template <class T>
	inline void read_impl(BitStream &bs, std::forward_list<T> &val) {
		val.clear();
		uint64_t sz;
		bs.Read(sz);
		for (uint64_t i = 0; i != sz; ++i) {
			T el;
			read_impl(bs, el);
			val.push_front(el);
		}
		val.reverse();
	}
	template <class T>
	inline void read_impl(BitStream &bs, std::deque<T> &val) {
		val.clear();
		uint64_t sz;
		bs.Read(sz);
		for (uint64_t i = 0; i != sz; ++i) {
			T el;
			read_impl(bs, el);
			val.push_back(el);
		}
	}
	template <class T>
	inline void read_impl(BitStream &bs, std::queue<T> &val) {
		val = {};
		uint64_t sz;
		bs.Read(sz);
		for (uint64_t i = 0; i != sz; ++i) {
			T el;
			read_impl(bs, el);
			val.push(el);
		}
	}
	template <class T>
	inline void read_impl(BitStream &bs, std::unique_ptr<T> &val) {
		val.reset();
		char sz = 0;
		bs.Read(sz);
		if (sz == 1) {
			T tmp;
			read_impl(bs, tmp);
			val.reset(new T(tmp));
		}
	}
	template <class T>
	inline void read_impl(BitStream &bs, std::shared_ptr<T> &val) {
		val.reset();
		char sz = 0;
		bs.Read(sz);
		if (sz == 1) {
			T tmp;
			read_impl(bs, tmp);
			val.reset(new T(tmp));
		}
	}
}

#include <map>

namespace skyseri
{
	namespace detail
	{
		class IFunc
		{
		public:
			virtual void Call(RakNet::BitStream &bs) = 0;
			virtual ~IFunc() = default;
		};

		class Func0 : public IFunc
		{
		public:
			Func0(std::function<void(void)> _f) : f(_f) {}
			void Call(RakNet::BitStream &bs) override {
				f();
			}
		private:
			const std::function<void(void)> f;
		};

		template<class T1>
		class Func1 : public IFunc
		{
		public:
			Func1(std::function<void(T1)> _f) : f(_f) {}
			void Call(RakNet::BitStream &bs) override {
				T1 t1;
				skyseri::read(bs, t1);
				f(t1);
			}
		private:
			const std::function<void(T1)> f;
		};

		template<class T1, class T2>
		class Func2 : public IFunc
		{
		public:
			Func2(std::function<void(T1, T2)> _f) : f(_f) {}
			void Call(RakNet::BitStream &bs) override {
				T1 t1;
				T2 t2;
				skyseri::read(bs, t1, t2);
				f(t1, t2);
			}
		private:
			const std::function<void(T1, T2)> f;
		};

		template<class T1, class T2, class T3>
		class Func3 : public IFunc
		{
		public:
			Func3(std::function<void(T1, T2, T3)> _f) : f(_f) {}
			void Call(RakNet::BitStream &bs) override {
				T1 t1;
				T2 t2;
				T3 t3;
				skyseri::read(bs, t1, t2, t3);
				f(t1, t2, t3);
			}
		private:
			const std::function<void(T1, T2, T3)> f;
		};

		template<class T1, class T2, class T3, class T4>
		class Func4 : public IFunc
		{
		public:
			Func4(std::function<void(T1, T2, T3, T4)> _f) : f(_f) {}
			void Call(RakNet::BitStream &bs) override {
				T1 t1;
				T2 t2;
				T3 t3;
				T4 t4;
				skyseri::read(bs, t1, t2, t3, t4);
				f(t1, t2, t3, t4);
			}
		private:
			const std::function<void(T1, T2, T3, T4)> f;
		};

		template<class T1, class T2, class T3, class T4, class T5>
		class Func5 : public IFunc
		{
		public:
			Func5(std::function<void(T1, T2, T3, T4, T5)> _f) : f(_f) {}
			void Call(RakNet::BitStream &bs) override {
				T1 t1;
				T2 t2;
				T3 t3;
				T4 t4;
				T5 t5;
				skyseri::read(bs, t1, t2, t3, t4, t5);
				f(t1, t2, t3, t4, t5);
			}
		private:
			const std::function<void(T1, T2, T3, T4, T5)> f;
		};

		template<class T1, class T2, class T3, class T4, class T5, class T6>
		class Func6 : public IFunc
		{
		public:
			Func6(std::function<void(T1, T2, T3, T4, T5, T6)> _f) : f(_f) {}
			void Call(RakNet::BitStream &bs) override {
				T1 t1;
				T2 t2;
				T3 t3;
				T4 t4;
				T5 t5;
				T6 t6;
				skyseri::read(bs, t1, t2, t3, t4, t5, t6);
				f(t1, t2, t3, t4, t5, t6);
			}
		private:
			const std::function<void(T1, T2, T3, T4, T5, T6)> f;
		};
	}

	using void_t = unsigned char;

	template <class ... Ts>
	void write_call(RakNet::BitStream &bs, string funcname, Ts ... ts) {
		skyseri::write(bs, funcname, ts ...);
	}

	class Runner
	{
	public:

		void Bind(string name, std::function<void()> f) {
			PreBind(name);
			funcs[name] = new detail::Func0(f);
		}

		template<class T1>
		void Bind(string name, std::function<void(T1)> f) {
			PreBind(name);
			funcs[name] = new detail::Func1<T1>(f);
		}

		template<class T1, class T2>
		void Bind(string name, std::function<void(T1, T2)> f) {
			PreBind(name);
			funcs[name] = new detail::Func2<T1, T2>(f);
		}

		template<class T1, class T2, class T3>
		void Bind(string name, std::function<void(T1, T2, T3)> f) {
			PreBind(name);
			funcs[name] = new detail::Func3<T1, T2, T3>(f);
		}

		template<class T1, class T2, class T3, class T4>
		void Bind(string name, std::function<void(T1, T2, T3, T4)> f) {
			PreBind(name);
			funcs[name] = new detail::Func4<T1, T2, T3, T4>(f);
		}

		template<class T1, class T2, class T3, class T4, class T5>
		void Bind(string name, std::function<void(T1, T2, T3, T4, T5)> f) {
			PreBind(name);
			funcs[name] = new detail::Func5<T1, T2, T3, T4, T5>(f);
		}

		template<class T1, class T2, class T3, class T4, class T5, class T6>
		void Bind(string name, std::function<void(T1, T2, T3, T4, T5, T6)> f) {
			PreBind(name);
			funcs[name] = new detail::Func6<T1, T2, T3, T4, T5, T6>(f);
		}

		void Call(RakNet::BitStream &bs) {
			string funcName;
			skyseri::read(bs, funcName);
			try {
				if (funcs.count(funcName) == 0) {
					throw std::logic_error("function not found");
				}
				funcs.at(funcName)->Call(bs);
			}
			catch (...) {
				// TODO: ���������� ������� ��������, ������ bs � �������� ���������
				throw;
			}
		}

		~Runner() {
			for (auto &p : funcs) delete p.second;
		}

	private:

		void PreBind(string name) {
			if (funcs.count(name)) throw std::logic_error("already binded");
		}

		std::map<string, detail::IFunc *> funcs;
	};
}