#pragma once

struct Movement
{
	NiPoint3 _pos;
	uint8_t _angleY = 0; // for bow sync, now unused
	uint8_t _angleZ = 0; // angleZ / 1.4
	uint8_t _direction = 0; // direction / 1.4
	uint8_t _flags = 0;
};
static_assert(sizeof Movement == 16, "");