#include "../stdafx.h"
#include "SimpleForm.h"

SimpleForm::SimpleForm(formid id, uint32_t flags) : TESForm(id, flags) 
{
}

void SimpleForm::Load(json from) {
	dat = from;
}

void SimpleForm::Save(json *to) {
	*to = dat;
}