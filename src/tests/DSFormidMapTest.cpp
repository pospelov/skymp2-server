#include "../stdafx.h"
#include "../DSFormidMap.h"

TEST_CASE("GetSegment", "[DSFormidMap]") {
	REQUIRE(GetSegment(0xFF000A11) == 0xFF);
	REQUIRE(GetSegment(0x00000228) == 0x00);
	REQUIRE(GetSegment(0x01000ACD) == 0x01);
}

TEST_CASE("DSFormidMap 1", "[DSFormidMap]") {
	DSFormidMap<string> map;
	map[0xFF000000] = "abc";
	map[0xFF000123] = "xyz";
	map[0x00000123] = "abcabc";
	map[0xBB000123] = "yandex waits for shura";

	for (int i = 0; i != 3; i++) {
		REQUIRE(map[0xFF000000] == "abc");
		REQUIRE(map[0xFF000123] == "xyz");
		REQUIRE(map[0x00000123] == "abcabc");
		REQUIRE(map[0xBB000123] == "yandex waits for shura");
	}

	REQUIRE(map.GetKeys() == std::vector<formid>{0xFF000000, 0xFF000123, 0x00000123, 0xBB000123});
}
/*
TEST_CASE("erase keys test", "[DSFormidMap]") {
	DSFormidMap<string> map;
	map[0x00000001] = "12214c";
	map[0x00000002] = "@ @ @ ! @";

	map.Erase(0x00000001);
	REQUIRE(map.GetKeys() == std::vector<formid>{0x00000002});
}

TEST_CASE("erase 2", "[DSFormidMap]") {
	DSFormidMap<string> map;
	map[0xFF000000] = "12214c";
	map[0xFF000123] = "@ @ @ ! @";
	map[0x00000123] = "ab214abc";
	map[0xBB000123] = "!(o_O)!";

	REQUIRE(map[0xFF000000] == "12214c");
	REQUIRE(map[0xFF000123] == "@ @ @ ! @");
	REQUIRE(map[0x00000123] == "ab214abc");
	REQUIRE(map[0xBB000123] == "!(o_O)!");

	REQUIRE(map.GetKeys() == std::vector<formid>{0xFF000000, 0xFF000123, 0x00000123, 0xBB000123});

	map.Erase(0xFF000000);
	map.Erase(0x00000123);
	REQUIRE(map.GetKeys() == std::vector<formid>{0xFF000123, 0xBB000123});
}*/