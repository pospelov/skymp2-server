#include "../stdafx.h"
#include "../FormManager.h"
#include "../forms/TESForm.h"

class TestTESForm : public TESForm
{
public:
	TestTESForm(string type, formid id, uint32_t flags) : TESForm(id, flags), type(type)
	{

	}

	string type;

	string GetType() const override { return type; };

	void Load(json from) override {};
	void Save(json *to) override {};
};

TEST_CASE("fm 1", "[FormManager]") {
	FormManager form;
	form.Register(std::make_unique<TestTESForm>("TEST", 0xAAAA, 0));
	REQUIRE(form.GetFormIDs() == vector<formid>{0xAAAA});
	REQUIRE(form.GetNumForms() == 1);

	form.Register(std::make_unique<TestTESForm>("QQQQ", 0xAAAB, 0));
	REQUIRE(form.GetFormIDs() == vector<formid>{0xAAAA, 0xAAAB});
	REQUIRE(form.GetNumForms() == 2);

	REQUIRE(form.GetFormIDs("TEST") == vector<formid>{0xAAAA});
	REQUIRE(form.GetFormIDs("QQQQ") == vector<formid>{0xAAAB});
}

TEST_CASE("fm 2", "[FormManager]") {
	FormManager form;
	form.Register(std::make_unique<TestTESForm>("TEST", 0x0A0A, 0));
	REQUIRE(form.GetFormIDs() == vector<formid>{0x0A0A});
	REQUIRE(form.GetNumForms() == 1);

	form.Register(std::make_unique<TestTESForm>("QQQQ", 0x0B0B, 0));
	REQUIRE(form.GetFormIDs() == vector<formid>{0x0A0A, 0x0B0B});
	REQUIRE(form.GetNumForms() == 2);

	form.Register(std::make_unique<TestTESForm>("QQQQ", 0x0C0C, 0));
	REQUIRE(form.GetFormIDs() == vector<formid>{0x0A0A, 0x0B0B, 0x0C0C});
	REQUIRE(form.GetNumForms() == 3);

	form.Register(std::make_unique<TestTESForm>("TEST", 0x0D0D, 0));
	REQUIRE(form.GetFormIDs() == vector<formid>{0x0A0A, 0x0B0B, 0x0C0C, 0x0D0D});
	REQUIRE(form.GetNumForms() == 4);

	REQUIRE(form.GetFormIDs("TEST") == vector<formid>{0x0A0A, 0x0D0D});
	REQUIRE(form.GetFormIDs("QQQQ") == vector<formid>{0x0B0B, 0x0C0C});

	REQUIRE(form[0x0A0A]);
	REQUIRE(form[0x0B0B]);
	REQUIRE(form[0x0C0C]);
	REQUIRE(form[0x0D0D]);

	REQUIRE(form[0x0A0A]->id == 0x0A0A); 
	REQUIRE(form[0x0B0B]->id == 0x0B0B);
	REQUIRE(form[0x0C0C]->id == 0x0C0C);
	REQUIRE(form[0x0D0D]->id == 0x0D0D);
}