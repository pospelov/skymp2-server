#include "../stdafx.h"
#include "../skyseri.h"

// TODO: std::array
// TODO: write(std::array) => read(vector)
// TODO: write(std::array) => read(std::list)
// TODO: write(vector) => read(std::array)
// TODO: write(std::list) => read(std::array)
// TODO: write(std::list/vector) => read(std::array) ������� ����������, ���� ������ ������� �� ������������� ���-�� ��. std::list/vector
// TODO: write(std::array<char, ...>) => read(string)
// TODO: write(const wchar_t *) => read(wstring)
// TODO: std::map
// TODO: std::set
// TODO: std::queue
// TODO: std::forward_list
// TODO: std::pair
// TODO: std::shared_ptr
// TODO: write(std::shared_ptr) => read(std::unique_ptr)

TEST_CASE("write and read simple types", "[skyseri]") {
	struct data
	{
		int8_t i8;
		int16_t i16;
		int32_t i32;
		int64_t i64;
		uint8_t u8;
		uint16_t u16;
		uint32_t u32;
		uint64_t u64;
		float f32;
		double f64;
	};

	data d = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	BitStream bs;
	skyseri::write(bs, d.i8, d.i16, d.i32, d.i64, d.u8, d.u16, d.u32, d.u64, d.f32, d.f64);
	d = {};
	skyseri::read(bs, d.i8, d.i16, d.i32, d.i64, d.u8, d.u16, d.u32, d.u64, d.f32, d.f64);

	REQUIRE(d.i8 == 1);
	REQUIRE(d.i16 == 2);
	REQUIRE(d.i32 == 3);
	REQUIRE(d.i64 == 4);
	REQUIRE(d.u8 == 5);
	REQUIRE(d.u16 == 6);
	REQUIRE(d.u32 == 7);
	REQUIRE(d.u64 == 8);
	REQUIRE(d.f32 == 9);
	REQUIRE(d.f64 == 10);
}

TEST_CASE("write and read string", "[skyseri]") {
	string str = "abc123123123123123123123123123123123123123", str2;
	BitStream bs;
	skyseri::write(bs, str);
	skyseri::read(bs, str2);
	str.clear();
	REQUIRE(str2 == "abc123123123123123123123123123123123123123");
}

TEST_CASE("write and read two strings", "[skyseri]") {
	string stra = "abc123123123123123123123123123123123123123", strb = "qweqweqweqweqwe", stra2, strb2;
	BitStream bs;
	skyseri::write(bs, stra, strb);
	skyseri::read(bs, stra2, strb2);
	stra.clear();
	strb.clear();
	REQUIRE(stra2 == "abc123123123123123123123123123123123123123");
	REQUIRE(strb2 == "qweqweqweqweqwe");
}

TEST_CASE("write(const char *) => read(string)", "[skyseri]") {
	const char *str1 = "sdfdbgfg";
	string str2;
	BitStream bs;
	skyseri::write(bs, str1);
	skyseri::read(bs, str2);
	REQUIRE(str1 == str2);
}
/*
TEST_CASE("write and read std::array", "[skyseri]") {
	std::array<int,3> a = { 1,2,3 }, b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);
	REQUIRE(a == b);
}*/

TEST_CASE("write and read vector", "[skyseri]") {
	std::vector<int> a = { 1,2,3 }, b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);
	REQUIRE(a == b);
}

TEST_CASE("write and read vector of strings", "[skyseri]") {
	std::vector<string> a = { "qwe", "rty", "1", "", "123456789012345678901234567890" }, b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);
	REQUIRE(a == b);
}

TEST_CASE("write and read vector<vector<string>>", "[skyseri]") {
	vector<vector<string>> a = { { "qqq","2343","qqq2" },{ "sdkjgfjidsjfsdfkl","2341" },{} }, b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);
	REQUIRE(a == b);
}

TEST_CASE("write and read std::list", "[skyseri]") {
	std::list<int> a = { 1,2,3,4,5 }, b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);
	REQUIRE(a == b);
}

TEST_CASE("write and read std::list<vector<int>>", "[skyseri]") {
	std::list<vector<int>> a = { {1,2,3},{4,5},{ } }, b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);
	REQUIRE(a == b);
}

TEST_CASE("write and read std::list<std::list<string>>", "[skyseri]") {
	std::list<std::list<string>> a = { { "ewr","wetewt","dfhdhh" },{ "sky","m11111111111111111asdASDASD ) ) ) ) 11 )111111111111111111111111111111111111p" }, {} }, b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);
	REQUIRE(a == b);
}

TEST_CASE("write and read std::list<std::list<int>>", "[skyseri]") {
	std::list<std::list<int>> a = { { 123, 1233, 444 },{ 63,12121212 },{} }, b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);
	REQUIRE(a == b);
}

TEST_CASE("write(std::list<string>) => read(vector<string>)", "[skyseri]") {
	std::list<string> a = { "1","223","ttryy","fafasf","never gonna give you up" };
	vector<string> b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);

	REQUIRE(a.size() == b.size());
	auto ptr1 = a.begin();
	auto ptr2 = b.begin();
	for (; ptr1 != a.end(); ptr1++, ptr2++) {
		REQUIRE(*ptr1 == *ptr2);
	}
}

TEST_CASE("write(std::list<string>) => read(vector<vector<char>>)", "[skyseri]") {
	std::list<string> a = { "1","223","ttryy","fafasf","never gonna give you up" };
	vector<vector<char>> b;
	BitStream bs;
	skyseri::write(bs, a);
	skyseri::read(bs, b);

	REQUIRE(a.size() == b.size());
	auto ptr1 = a.begin();
	auto ptr2 = b.begin();
	for (; ptr1 != a.end(); ptr1++, ptr2++) {
		auto teststr = string(ptr2->data(), ptr2->size());
		REQUIRE(*ptr1 == teststr);
	}
}

TEST_CASE("read() throws exception when data is corrupted", "[skyseri]") {
	int a = 10, b, c;
	BitStream bs;
	skyseri::write(bs, a);
	try {
		skyseri::read(bs, b, c);
	}
	catch (std::exception &e) {
		REQUIRE(1);
		return;
	}
	REQUIRE(0);
}

TEST_CASE("write and read complicated types", "[skyseri]") {
	struct data
	{
		int8_t i8;
		int16_t i16;
		int32_t i32;
		int64_t i64;
		string str;
		vector<string> vstr;
		vector<vector<string>> vvstr;
		vector<int16_t> vi16;
		uint8_t u8;
		uint16_t u16;
		uint32_t u32;
		uint64_t u64;
		float f32;
		double f64;

	};

	data d = { 1, 2, 3, 4, "SkyMP", {"SkyMp"," ","","Together","2232121312421441244!!!!&&"},{{"gde","spa","sibo","?"}, {"spasibo","za","project"}, {"",""}},{-1,2,-3,4,5,6,-2132}, 5, 6, 7, 8, 9.2f, 10.63 };

	BitStream bs;
	skyseri::write(bs, d.i8, d.i16, d.i32, d.i64, d.str, d.vstr, d.vvstr, d.vi16, d.u8, d.u16, d.u32, d.u64, d.f32, d.f64);
	d = {};
	skyseri::read(bs, d.i8, d.i16, d.i32, d.i64, d.str, d.vstr, d.vvstr, d.vi16, d.u8, d.u16, d.u32, d.u64, d.f32, d.f64);

	REQUIRE(d.i8 == 1);
	REQUIRE(d.i16 == 2);
	REQUIRE(d.i32 == 3);
	REQUIRE(d.i64 == 4);
	REQUIRE(d.str == "SkyMP");
	REQUIRE(d.vstr == vector<string>({ "SkyMp"," ","","Together","2232121312421441244!!!!&&" }));
	REQUIRE(d.vvstr == vector<vector<string>>({ { "gde","spa","sibo","?" },{ "spasibo","za","project" }, { "","" } }));
	REQUIRE(d.vi16 == vector<int16_t>({ -1,2,-3,4,5,6,-2132 }));
	REQUIRE(d.u8 == 5);
	REQUIRE(d.u16 == 6);
	REQUIRE(d.u32 == 7);
	REQUIRE(d.u64 == 8);
	REQUIRE(d.f32 == 9.2f);
	REQUIRE(d.f64 == 10.63);
}

TEST_CASE("1 arg", "[skyseri]") {
	skyseri::Runner r;
	int calls = 0;

	r.Bind<int>("Foo", [&](int a) {
		REQUIRE(a == 123);
		++calls;
	});

	BitStream bs;
	skyseri::write_call(bs, "Foo", 123);
	r.Call(bs);
	REQUIRE(calls == 1);
}

TEST_CASE("2 args", "[skyseri]") {
	skyseri::Runner r;
	int calls = 0;

	r.Bind<int, string>("Foo", [&](int a, string b) {
		REQUIRE(a == 321);
		REQUIRE(b == "qwerty");
		++calls;
	});

	BitStream bs;
	skyseri::write_call(bs, "Foo", 321, "qwerty");
	r.Call(bs);
	REQUIRE(calls == 1);
}

TEST_CASE("3 args", "[skyseri]") {
	skyseri::Runner r;
	int calls = 0;

	r.Bind<int, string, double>("Foo", [&](int a, string b, double c) {
		REQUIRE(a == -1);
		REQUIRE(b == "!!!");
		REQUIRE(c == 100.1);
		++calls;
	});

	BitStream bs;
	skyseri::write_call(bs, "Foo", -1, "!!!", 100.1);
	r.Call(bs);
	REQUIRE(calls == 1);
}

TEST_CASE("4 args", "[skyseri]") {
	skyseri::Runner r;
	int calls = 0;

	r.Bind<int, string, double, char>("Foo", [&](int a, string b, double c, char d) {
		REQUIRE(a == -1);
		REQUIRE(b == "!!!");
		REQUIRE(c == 100.1);
		REQUIRE(d == 'q');
		++calls;
	});

	BitStream bs;
	skyseri::write_call(bs, "Foo", -1, "!!!", 100.1, 'q');
	r.Call(bs);
	REQUIRE(calls == 1);
}

TEST_CASE("5 args", "[skyseri]") {
	skyseri::Runner r;
	int calls = 0;

	r.Bind<int, string, double, char, vector<int>>("Foo", [&](int a, string b, double c, char d, vector<int> e) {
		REQUIRE(a == -1);
		REQUIRE(b == "!!!");
		REQUIRE(c == 100.1);
		REQUIRE(d == 'q');
		REQUIRE(e == vector<int>({1, 2, 3}));
		++calls;
	});

	BitStream bs;
	skyseri::write_call(bs, "Foo", -1, "!!!", 100.1, 'q', vector<int>({ 1,2,3 }));
	r.Call(bs);
	REQUIRE(calls == 1);
}

TEST_CASE("6 args", "[skyseri]") {
	skyseri::Runner r;
	int calls = 0;

	r.Bind<int, string, double, char, vector<int>, vector<string>>("Foo", [&](int a, string b, double c, char d, vector<int> e, vector<string> f) {
		REQUIRE(a == -1);
		REQUIRE(b == "!!!");
		REQUIRE(c == 100.1);
		REQUIRE(d == 'q');
		REQUIRE(e == vector<int>({1, 2, 3}));
		REQUIRE(f == vector<string>({"kura", "and", "shura"}));
		++calls;
	});

	BitStream bs;
	skyseri::write_call(bs, "Foo", -1, "!!!", 100.1, 'q', vector<int>({ 1,2,3 }), vector<const char *>({ "kura", "and", "shura" }));
	r.Call(bs);
	REQUIRE(calls == 1);
}
