#include "../stdafx.h"
#include "../EsmParser.h"

template <class T>
void AddField(espm::Record *r, const char *type, T value) {
	espm::Field &f = *new espm::Field;
	memcpy(&f.type, type, 4);
	f.dataSize = sizeof value;
	auto valueCopy = new T(value);
	f.data = (char *)valueCopy;
	r->fields.push_back(f);
}

espm::Record *NewRecord(const char *type, formid id, uint32_t flags) {
	espm::Record *r = new espm::Record;
	memcpy(r->type, type, 4);
	r->id = id;
	r->flags = flags;
	return r;
}

TEST_CASE("can parse CELL", "[EsmParser]") {
	auto r = NewRecord("CELL", 0x1, 0);
	AddField(r, "XLCN", formid(0x2));
	AddField(r, "XCLC", array<int32_t, 2>({ -21, 42 }));
	AddField(r, "DATA", uint8_t(0));

	EsmParser::ParseRecord(*r, new formid(0x3), [](FormData document) {
		REQUIRE(document.fields == json{
			{ "locationid", 0x2 },
			{ "parentid", 0x3 },
			{ "x", -21 },
			{ "y", 42 },
			{ "interior", false },
			{ "publicArea", false }
		});
		REQUIRE(document.id == 0x1);
		REQUIRE(document.flags == 0);
		REQUIRE(document.type == "CELL");
	});
}

TEST_CASE("can parse REFR", "[EsmParser]") {
	auto r = NewRecord("REFR", 0xA, 0);
	AddField(r, "NAME", formid(0x2));
	AddField(r, "DATA", array<NiPoint3, 2>({ NiPoint3(1, 2, 3), NiPoint3(4, 5, 6) }));

	EsmParser::ParseRecord(*r, new formid(0xB), [](FormData document) {

		REQUIRE(document.fields == json{
			{ "parentid", 0xB },
			{ "baseid", 0x2 },
			{ "pos", NiPoint3(1, 2, 3) },
			{ "rot", NiPoint3(4, 5, 6) }
		});
		REQUIRE(document.id == 0xA);
		REQUIRE(document.flags == 0);
		REQUIRE(document.type == "REFR");
	});
}

TEST_CASE("can not parse unknown", "[EsmParser]") {
	auto r = NewRecord("NIKA", 0xA, 0);
	EsmParser::ParseRecord(*r, new formid(0), [](FormData document) {
		REQUIRE(false);
	});
}