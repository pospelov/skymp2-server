#pragma once
#include "../forms/TESForm.h"

class SimpleForm : public TESForm
{
public:
	SimpleForm(formid id, uint32_t flags);

	string GetType() const override { return "TEST"; };

	void Load(json from) override;
	void Save(json *to) override;

private:
	json dat;
};