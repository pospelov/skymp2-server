#define main tests_main

#define CATCH_CONFIG_MAIN  

#include "../stdafx.h"
#include "tests.h"

TEST_CASE("1 and 0 are not equal", "[example]") {
	REQUIRE(1 != 0);
}