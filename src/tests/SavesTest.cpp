#include "../stdafx.h"
#include "../Saves.h"

TEST_CASE("empty Saves has no data", "[Saves]") {
	Saves saves({});
	REQUIRE(saves.CommitChanges().empty());
	REQUIRE(saves.GetFormIDs().empty());
	REQUIRE(saves.GetFieldsForAll().empty());
}

TEST_CASE("GetTypeAndFlags works", "[Saves]") {
	Saves::Map map;
	map["FOO_"] = json::object();
	map["FOO_"]["FF000000"] = json{
		{ "_FLAGS", 111 },
		{ "_TYPE", "FOO_" }
	};
	Saves saves(map);
	REQUIRE(saves.GetTypeAndFlags(0xFF000000) == TypeAndFlags{ "FOO_", 111 });
}

TEST_CASE("GetTypeAndFlags is not working for <0xFF000000", "[Saves]") {
	string err;
	try {
		Saves saves({});
		saves.GetTypeAndFlags(0xA);
	}
	catch (std::exception &e) {
		err = e.what();
	}
	REQUIRE(err == "bad id");
}

TEST_CASE("GetTypeAndFlags is not working for unknown id", "[Saves]") {
	string err;
	try {
		Saves saves({});
		saves.GetTypeAndFlags(0xFF000000);
	}
	catch (std::exception &e) {
		err = e.what();
	}
	REQUIRE(err == "unknown id");
}

TEST_CASE("GetFormIDs", "[Saves]") {
	const std::set<formid> fooForms = { 0xA, 0xB,0xC,0xD },
		barForms = { 0x11,0x22,0x33,0x44 };

	Saves::Map m;
	for (auto f : fooForms) m["FOO_"][to_string_hex(f)] = json::object();
	for (auto f : barForms) m["BAR_"][to_string_hex(f)] = json::object();
	Saves s(m);

	REQUIRE(s.GetFormIDs() == std::set<formid>{ 0xA, 0xB, 0xC, 0xD, 0x11, 0x22, 0x33, 0x44 });
}

TEST_CASE("GetFieldsFor", "[Saves]") {
	Saves::Map m;
	m["FOO_"] = json::object();
	m["FOO_"]["00000CAC"] = json{
		{ "key", "value" },
	};
	Saves s(m);
	REQUIRE(s.GetFieldsForAll()[0xCAC] == json{ { "key", "value" } });
}

TEST_CASE("GetFieldsFor and GetTypeAndFlags >=0xFF000000", "[Saves]") {
	Saves::Map m;
	m["FOO_"] = json::object();
	m["FOO_"]["0xFF000000"] = json{
		{ "key", "value" },
		{ "_TYPE", "FOO_" },
		{ "_FLAGS", 1 }
	};
	Saves s(m);
	REQUIRE(s.GetFieldsForAll()[0xFF000000] == json{ { "key", "value" } });
	REQUIRE(s.GetTypeAndFlags(0xFF000000) == TypeAndFlags({ "FOO_", 1 }));
}

TEST_CASE("InsertFormFields invalid object", "[Saves]") {
	string err;
	try {
		Saves s({});
		s.InsertFormFields(0xFF000000, json::array(), { "FOO_", 0 });
	}
	catch (std::exception &e) {
		err = e.what();
	}
	REQUIRE(err == "object expected");
}

TEST_CASE("InsertFormFields", "[Saves]") {
	Saves s({});
	s.InsertFormFields(0xFF000000, json({ { "key", "value" } }), { "FOO_", 2048 });
	REQUIRE(s.GetFieldsForAll()[0xFF000000] == json{ { "key", "value" } });
	REQUIRE(s.GetTypeAndFlags(0xFF000000) == TypeAndFlags({ "FOO_", 2048 }));
}

TEST_CASE("all", "[Saves]") {
	// ���������� �����
	// ��� ����� �� ��� �� ������ FOO_.json � BAR_.json ���������
	Saves::Map map;
	map["FOO_"] = json::object();
	map["FOO_"]["0000000A"] = json{
		{ "abc", "xyz" }
	};
	map["FOO_"]["0000000B"] = json{
		{ "abc", "abv" }
	};
	map["FOO_"]["0000000C"] = json{
		{ "abc", "huh" }
	};
	map["BAR_"] = json::object();
	map["BAR_"]["0000001A"] = json{
		{ "abc", "xyz" }
	};
	map["BAR_"]["0000001B"] = json{
		{ "abc", "abv" }
	};
	map["BAR_"]["0000001C"] = json{
		{ "abc", "huh" }
	};

	Saves saves(map);

	auto ids = saves.GetFormIDs();
	REQUIRE(ids == std::set<formid>({
		0xA, 0xB, 0xC, 0x1A, 0x1B, 0x1C
	}));

	for (auto id : ids) {
		auto fields = saves.GetFieldsForAll()[id];

		REQUIRE(fields.is_object());
		REQUIRE(fields["abc"].is_string());

		// ������, ������� ������ ����� � ����� ��������� ���, ������� �� ����������
		bool found = fields == map["BAR_"][to_string_hex(id)] || fields == map["FOO_"][to_string_hex(id)];
		REQUIRE(found);
	}

	// 0xA �� ��� �����, ����� ���. ��������� ���.
	REQUIRE(saves.GetFieldsForAll()[0xA] == json{ { "abc", "xyz" } });
	REQUIRE(saves.CommitChanges().empty());

	// ������������ 0xA � �������, ��� �����.
	saves.InsertFormFields(0xA, json{
		{ "abc", "xy" }
	}, { "FOO_", 0 });
	REQUIRE(saves.GetFieldsForAll()[0xA] == json{ { "abc", "xy" } });
	auto commit = saves.CommitChanges();
	REQUIRE(commit.size() == 1);
	REQUIRE(commit.count("FOO_"));
	REQUIRE(commit["FOO_"] == json{
		{ "0000000A", json{ { "abc", "xy" } } },
		{ "0000000B", json{ { "abc", "abv" } } },
		{ "0000000C", json{ { "abc", "huh" } } }
	});
	REQUIRE(saves.CommitChanges().empty());

	REQUIRE(saves.GetFieldsForAll().size() == 6); // �� ������ ������ ��������, ��� ��������� ����� ������ �� ������

												  // ��� runtime-�����

	saves.InsertFormFields(0xFF000000, {
		{ "answer", 42 }
	}, { "BAR_", 123 });
	REQUIRE(saves.GetFieldsForAll().size() == 7);
	REQUIRE(saves.GetFieldsForAll()[0xFF000000]["answer"].get<int>() == 42);
	REQUIRE(saves.GetTypeAndFlags(0xFF000000) == TypeAndFlags{ "BAR_", 123 });
}