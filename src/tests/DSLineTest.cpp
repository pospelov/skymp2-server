#include "../stdafx.h"
#include "../DSLine.h"

TEST_CASE("DSLine 1", "[DSFormidMap]") {
	DSLine<string> line;
	line[1] = "1";
	line[200] = "200";
	line[0] = "0";
	line[-1000] = "-1000";
	line[-300300] = "-300300";
	for (int i = 0; i != 3; ++i) {
		REQUIRE(line[1] == "1");
		REQUIRE(line[200] == "200");
		REQUIRE(line[0] == "0");
		REQUIRE(line[-1000] == "-1000");
		REQUIRE(line[-300300] == "-300300");
	}
}