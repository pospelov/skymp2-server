#include "stdafx.h"
#include "Saves.h"

#include "forms/TESForm.h"

struct Saves::Impl
{
	Map data, uncommited;

	// only for runtime forms
	std::map<formid, TypeAndFlags> typePlusFlags;
};

Saves::Saves(Map data) {
	pImpl = new Impl;
	pImpl->data = data;

	this->InitTypesAndFlags();
}

void Saves::InitTypesAndFlags() {
	for (auto &pair : pImpl->data) {
		for (const auto &j : json::iterator_wrapper(pair.second)) {
			formid id = strtoul(j.key().data(), nullptr, 16);
			if (j.value().count("_FLAGS") && j.value().count("_TYPE")) {
				pImpl->typePlusFlags[id] = { j.value().at("_TYPE").get<string>(), j.value().at("_FLAGS").get<uint32_t>() };
			}
		}
	}
}

Saves::~Saves() {
	delete pImpl; pImpl = nullptr;
}

Saves::Map Saves::CommitChanges() {
	Map res;
	std::swap(res, pImpl->uncommited);
	assert(pImpl->uncommited.empty());
	return res;
}

void Saves::InsertFormFields(formid id, json fields, TypeAndFlags taf) {
	if (fields.is_null()) return;
	if (fields.is_object() == false) throw std::logic_error("object expected");
	if (fields == json::object()) return;

	auto &j = pImpl->data[taf.type][to_string_hex(id)];
	if (j.is_object() == false) j = json::object();
	extendJson(j, fields);
	if (id >= 0xFF000000) {
		j["_TYPE"] = taf.type;
		j["_FLAGS"] = taf.flags;

		pImpl->typePlusFlags[id] = { taf.type, taf.flags };
	}

	pImpl->uncommited[taf.type] = pImpl->data[taf.type];
}

std::map<formid, json> Saves::GetFieldsForAll() const {
	// TODO: ���������� res, � �� ������� ������ ��� (+ ����������� ����, ����� ��������, ��������, InsertFormFields)
	std::map<formid, json> res;
	for (auto &pair : pImpl->data) {
		auto bigStrangeJson = pair.second;
		for (const auto &j : json::iterator_wrapper(bigStrangeJson)) {
			const formid id = strtoul(j.key().data(), nullptr, 16);
			json value = j.value();
			// ������� ����������� ����, ������� ���������������� ��� ������ �� ������
			value.erase("_TYPE");
			value.erase("_FLAGS");
			res[id] = value;
			assert(id > 0);
			assert(value.is_object());
			assert(value.count("_TYPE") == 0);
			assert(value.count("_FLAGS") == 0);
		}
	}
	return res;
}

void Saves::Erase(formid id) {
	pImpl->data.erase(to_string_hex(id));
	auto type = pImpl->typePlusFlags[id].type;
	assert(type.size() != 0);
	pImpl->typePlusFlags.erase(id);

	pImpl->uncommited[type] = pImpl->data[type];
}

TypeAndFlags Saves::GetTypeAndFlags(formid id) const {
	if (id < 0xFF000000) throw std::logic_error("bad id");
	// TODO: O(1)
	try {
		return pImpl->typePlusFlags.at(id);
	}
	catch (...) {
		throw std::logic_error("unknown id");
	}
}

std::set<formid> Saves::GetFormIDs() {
	std::set<formid> ids;
	for (auto it = pImpl->data.begin(); it != pImpl->data.end(); ++it) {
		auto v = *it;
		for (const auto &j : json::iterator_wrapper(v.second)) {
			ids.insert(strtoul(j.key().data(), nullptr, 16));
		}
	}
	return ids;
}