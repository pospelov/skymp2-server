#include "stdafx.h"
#include "Server.h"

#include "skyseri.h"
#include "Saves.h"
#include "EsmParser.h"
#include "FormManager.h"
#include "FormFactory.h"
#include "forms/TESForm.h"

#include "../RakNet/src/RakPeerInterface.h"

namespace helper
{
	void SaveJson(fs::path p, json j) {
		fs::ofstream(p) << j.dump(2) << std::endl;
	}

	json LoadJson(fs::path p) {
		fs::ifstream t(p);
		string str((std::istreambuf_iterator<char>(t)),
			std::istreambuf_iterator<char>());
		return json::parse(str);
	}
}

enum {
	NUM_SOCKETS = 10
};

struct Server::Impl
{
	vector<std::unique_ptr<TESFile>> files;
	std::unique_ptr<Saves> saves;
	boost::lockfree::queue<Saves::Map *, boost::lockfree::capacity<60000>> commits;
	FormManager fm;
	RakNet::SocketDescriptor socket[NUM_SOCKETS];
	std::shared_ptr<RakNet::RakPeerInterface> peer = nullptr;
	std::thread *commitsWriter = nullptr;
	bool runThreads = true;

	void LoadEsm();
	int LoadSaves();
	void ParseEsmAndSaves();
	void InitRakNetPeer(Config cfg);
	void Save(TESForm *form);
	void FlushSave();
};

Server::Server(Config cfg) : config(cfg) {

	if (cfg.maxPlayers <= 0 || cfg.maxPlayers > MAX_PLAYERS) throw std::logic_error("maxPlayers must be between 1 to 2000");
	if (cfg.port > 65535 || cfg.port <= 0) throw std::logic_error("port must be between 1 and 65535");

	pImpl = new Impl;

	printf("Constructing the peer (port=%d, maxPlayers=%d)\n", cfg.port, cfg.maxPlayers);
	pImpl->InitRakNetPeer(cfg);

	printf("Loading masters and plugins\n");
	pImpl->LoadEsm();
	printf("Loading saves\n");
	int numSavefiles = pImpl->LoadSaves();
	printf("Loaded %d save files\n", numSavefiles);
	printf("Parsing\n");
	pImpl->ParseEsmAndSaves();
	printf("Done\n");

	auto worlds = pImpl->fm.GetFormIDs("WRLD");
	auto cells = pImpl->fm.GetFormIDs("CELL");
	auto refrs = pImpl->fm.GetFormIDs("REFR");
	printf("WLRD: %d; CELL %d; REFR %d;\n", (int)worlds.size(), (int)cells.size(), (int)refrs.size());

	pImpl->commitsWriter = new std::thread([this] {
		while (pImpl->runThreads) {
			std::this_thread::sleep_for(1000ms);
			Saves::Map *commit;
			while (pImpl->commits.pop(commit)) {
				assert(commit);
				for (auto &pair : *commit) {
					helper::SaveJson(fs::path("saves") / (pair.first + ".json"), pair.second);
				}
				delete commit;
			}
		}
	});
}

void Server::Impl::LoadEsm() {
	auto fileNames = { "Skyrim.esm" };
	for (auto fName : fileNames) {
		auto path = fs::path("skyrim") / "data" / fName;
		files.emplace_back(
			new TESFile(path, espm::Settings("tes5"), true, false)
		);
	}
}

int Server::Impl::LoadSaves() {
	Saves::Map data;
	int count = 0;
	for (auto &p : fs::directory_iterator("saves")) {

		
		auto str = p.path().string();
		// str: "blabla.json" => "blabla"
		// TODO: ������� ��� ��-����������� �/��� �� � ��������� ������� � ������� ������� �
		for (size_t i = 0; i != str.size(); ++i) {
			if (str[i] == '.') str[i] = 0;
		}
		str = str.data();

		data[str] = helper::LoadJson(p.path());
		++count;
	}
	saves.reset(new Saves(data));
	return count;
}

void Server::Impl::ParseEsmAndSaves() {
	auto savedFields = saves->GetFieldsForAll();
	for (auto &file : files) {
		EsmParser::Parse(*file, [&](FormData formData) {
			auto form = FormFactory::NewEmptyForm(formData.type, formData.id, formData.flags);
			if (form) {
				form->Load(formData.fields);
				form->Load(savedFields[form->id]);
				fm.Register(std::move(form));
			}
			savedFields.erase(formData.id);
		});
	}
	for (auto &p : savedFields) {
		FormData fd;
		fd.fields = p.second;
		fd.id = p.first;
		fd.flags = saves->GetTypeAndFlags(fd.id).flags;
		fd.type = saves->GetTypeAndFlags(fd.id).type;

		auto form = FormFactory::NewEmptyForm(fd.type, fd.id, fd.flags);
		if (form) {
			form->Load(fd.fields);
			fm.Register(std::move(form));
		}
	}
}

void Server::Impl::InitRakNetPeer(Config cfg) {
	std::shared_ptr<RakNet::RakPeerInterface> newPeer(RakNet::RakPeerInterface::GetInstance(), [=](auto *peer) {
		RakNet::RakPeerInterface::DestroyInstance(peer);
	});
	peer = newPeer;
	auto res = peer->Startup(cfg.maxPlayers, socket, NUM_SOCKETS);
	if (res != RakNet::StartupResult::RAKNET_STARTED) throw std::logic_error("unable to startup");

	peer->SetMaximumIncomingConnections(cfg.maxPlayers);//�� ��������
}

void Server::Impl::Save(TESForm *form) {
	json fields = json::object();
	form->Save(&fields);
	saves->InsertFormFields(form->id, fields, { form->GetType(), form->flags });
}

void Server::Impl::FlushSave() {
	assert(saves);
	auto commit = new Saves::Map(saves->CommitChanges());
	commits.push(commit);
}

Server::~Server() {
	pImpl->runThreads = false;
	assert(pImpl->commitsWriter->joinable());
	pImpl->commitsWriter->join();
	delete pImpl; pImpl = nullptr;
}

void Server::Tick() {
}