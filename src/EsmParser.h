#pragma once
#include "FormData.h"

namespace EsmParser
{
	using Callback = function<void(FormData)>;

	void Parse(TESFile &src, Callback);
	void ParseGroup(espm::Group &, Callback);
	void ParseRecord(espm::Record &, void *parentlabel, Callback);
}