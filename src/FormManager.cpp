#include "stdafx.h"
#include "FormManager.h"
#include "FormData.h"
#include "forms/TESForm.h"
#include "DSFormidMap.h"

struct FormManager::Impl
{
	DSFormidMap<std::unique_ptr<TESForm>>forms;
};

FormManager::FormManager() {
	pImpl = new Impl;
}

FormManager::~FormManager() {
	delete pImpl; pImpl = nullptr;
}

TESForm *FormManager::operator[](formid formid) const {
	return pImpl->forms[formid].get();
}

size_t FormManager::GetNumForms() const {
	// TODO: O(1)
	return pImpl->forms.GetSize();
}

vector<formid> FormManager::GetFormIDs(string type) const {
	// TODO: O(1)
	vector<formid> keys = pImpl->forms.GetKeys();
	if (type == "") return keys;

	vector<formid> result = {};
	for (int i = 0; i < keys.size(); i++)
	{
		if (pImpl->forms[keys[i]].get()->GetType() == type) result.push_back(keys[i]);
	}
	return result;
}

void FormManager::Register(unique_ptr<TESForm> form) {
	//printf("Registering %x\n", form->id);
	assert((*this)[form->id] == nullptr);
    pImpl->forms[form->id] = std::move(form);
}