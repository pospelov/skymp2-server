#pragma once

struct Config
{
	int maxPlayers;
	int port;
};

class Server : noncopyable
{
public:
	explicit Server(Config config);
	~Server();

	void Tick();

	const Config config;

private:
	enum { MAX_PLAYERS = 2000 };
	struct Impl;
	Impl *pImpl;
};